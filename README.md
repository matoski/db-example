[//]: # (spell-checker:disable)

# Interview Task

This is a simple test. Relax. Don’t overthink it. The question is written from an OOP perspective; if OOP isn’t your preferred paradigm please solve the problem in a way that would handle the same concerns listed below.

We’re looking for good design and demonstration of engineering concepts. Functioning code is not a requirement. In your favourite language and programming paradigm write the following.

* Write an abstract class or interface that models a SQL database connection.
  * You may build a single interface or compose multiple areas of concerns.
  * You can make some assumptions that we’re working with a relational database schema.

* Write a factory to create two different types of database connections
  * They don’t have to be real database engines. Make some up, but demonstrate how a factory deals with differences in concrete implementations.

* Implement a class that models some entity (e.g. cat, car, person, foo or bar etc) that will be persisted to a database created by your factory.
  * Your class should demonstrates dependency injection
  * If you think dependency model is wrong, model it some other way and tell us why your way is better.

* Write a test that tests your classes
  * Demonstrate how mocking works

[//]: # (spell-checker:enable)

# Technical Solution

![picture](db-example.png)

# Instructions

1. Install and run [Docker](https://www.docker.com/get-started)
1. Run with

```bash
docker-compose up --build
```

This will start [PostgreSQL](https://www.postgresql.org/) database, build, run tests, an run db-example. You should see this output.

```text
postgres_1    | PostgreSQL init process complete; ready for start up.
postgres_1    |
postgres_1    | 2019-01-05 00:47:14.414 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
postgres_1    | 2019-01-05 00:47:14.414 UTC [1] LOG:  listening on IPv6 address "::", port 5432
postgres_1    | 2019-01-05 00:47:14.419 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
postgres_1    | 2019-01-05 00:47:14.435 UTC [50] LOG:  database system was shut down at 2019-01-05 00:47:14 UTC
postgres_1    | 2019-01-05 00:47:14.441 UTC [1] LOG:  database system is ready to accept connections
db-example_1  | Mercedes Benz with registration number DW69XLO.
db-example_1  | Aston Martin with registration number MATO1.
db-example_1  | BMW Z4 with registration number FD19YIL.
db-example_db-example_1 exited with code 0
```
