
#build stage
FROM golang:alpine AS builder
ENV GO111MODULE=on
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .
RUN apk add --no-cache git
RUN go get -d -v ./...
RUN go test -v ./...
RUN go install -v ./...

#final stage
FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/bin/db-example /app
ENTRYPOINT ./app
LABEL Name=db-example Version=1.0.0
