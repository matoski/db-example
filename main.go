package main

import (
	"fmt"
	"io"
	"log"
	"os"

	"bitbucket.org/matoski/db-example/models"
)

func main() {
	conf := map[string]string{
		"DATASTORE":              os.Getenv("DATASTORE"),
		"DATASTORE_POSTGRES_DSN": os.Getenv("DATASTORE_POSTGRES_DSN"),
	}
	ds, err := models.CreateDatastore(conf)
	if err != nil {
		log.Panic(err)
	} else {
		log.Printf("created datastore with engine %s\n", ds.Name())
	}

	AppLogic(os.Stdout, ds)
}

// AppLogic dummy logic.
func AppLogic(w io.Writer, ds models.Datastore) {
	c1 := &models.Car{
		RegNumber: "DW69XLO",
		Make:      "Mercedes",
		Model:     "Benz",
	}
	c2 := &models.Car{
		RegNumber: "MATO1",
		Make:      "Aston",
		Model:     "Martin",
	}
	c3 := &models.Car{
		RegNumber: "FD19YIL",
		Make:      "BMW",
		Model:     "Z4",
	}

	ds.AddCar(c1)
	ds.AddCar(c2)
	ds.AddCar(c3)

	cars, _ := ds.GetCars()
	for _, c := range cars {
		fmt.Fprintf(w, "%s\n", c.ToString())
		ds.RemoveCar(c)
	}
}
