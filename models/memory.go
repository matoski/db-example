package models

import (
	"sync"
)

// MemoryDatastore implements the XDatastore interfaces.
type MemoryDatastore struct {
	*sync.RWMutex
	Cars map[string]*Car
	Cats map[string]*Cat
}

// Name returns the name of the Datastore.
func (mds *MemoryDatastore) Name() string {
	return "memory"
}

//#region implement CarDatastore interface

// AddCar inserts the Car in the Datastore.
func (mds *MemoryDatastore) AddCar(c *Car) error {
	mds.RWMutex.Lock()
	defer mds.RWMutex.Unlock()
	if _, dup := mds.Cars[c.RegNumber]; dup {
		return CarAlreadyExistsError
	}
	mds.Cars[c.RegNumber] = c
	return nil
}

// FindCar returns the Car object for given regNumber.
func (mds *MemoryDatastore) FindCar(regNumber string) (*Car, error) {
	mds.RWMutex.RLock()
	defer mds.RWMutex.RUnlock()
	if c, dup := mds.Cars[regNumber]; dup {
		return c, nil
	}
	return nil, CarNotFoundError
}

// UpdateCar will update the Datastore with the Car object.
func (mds *MemoryDatastore) UpdateCar(c *Car) error {
	mds.RWMutex.Lock()
	defer mds.RWMutex.Unlock()
	if _, dup := mds.Cars[c.RegNumber]; dup {
		mds.Cars[c.RegNumber] = c
		return nil
	}
	return CarNotFoundError
}

// RemoveCar removes the Car from the Datastore.
func (mds *MemoryDatastore) RemoveCar(c *Car) error {
	mds.RWMutex.Lock()
	defer mds.RWMutex.Unlock()
	if _, dup := mds.Cars[c.RegNumber]; dup {
		delete(mds.Cars, c.RegNumber)
		return nil
	}
	return CarNotFoundError
}

// GetCars returns all of the Cars from the Datastore.
func (mds *MemoryDatastore) GetCars() ([]*Car, error) {
	mds.RWMutex.RLock()
	defer mds.RWMutex.RUnlock()
	cars := make([]*Car, 0)
	for _, c := range mds.Cars {
		cars = append(cars, c)
	}
	return cars, nil
}

//#endregion

//#region implement CatDatastore interface

// AddCat inserts the Cat in the Datastore.
func (mds *MemoryDatastore) AddCat(c *Cat) error {
	mds.RWMutex.Lock()
	defer mds.RWMutex.Unlock()
	if _, dup := mds.Cats[c.ID]; dup {
		return CatAlreadyExistsError
	}
	mds.Cats[c.ID] = c
	return nil
}

// FindCat returns the Cat object for given regNumber.
func (mds *MemoryDatastore) FindCat(regNumber string) (*Cat, error) {
	mds.RWMutex.RLock()
	defer mds.RWMutex.RUnlock()
	if c, dup := mds.Cats[regNumber]; dup {
		return c, nil
	}
	return nil, CatNotFoundError
}

// UpdateCat will update the Datastore with the Cat object.
func (mds *MemoryDatastore) UpdateCat(c *Cat) error {
	mds.RWMutex.Lock()
	defer mds.RWMutex.Unlock()
	if _, dup := mds.Cats[c.ID]; dup {
		mds.Cats[c.ID] = c
		return nil
	}
	return CatNotFoundError
}

// RemoveCat removes the Cat from the Datastore.
func (mds *MemoryDatastore) RemoveCat(c *Cat) error {
	mds.RWMutex.Lock()
	defer mds.RWMutex.Unlock()
	if _, dup := mds.Cats[c.ID]; dup {
		delete(mds.Cats, c.ID)
		return nil
	}
	return CatNotFoundError
}

// GetCats returns all of the Cats from the Datastore.
func (mds *MemoryDatastore) GetCats() ([]*Cat, error) {
	mds.RWMutex.RLock()
	defer mds.RWMutex.RUnlock()
	cats := make([]*Cat, 0)
	for _, c := range mds.Cats {
		cats = append(cats, c)
	}
	return cats, nil
}

//#endregion

// NewMemoryDatastore creates MemoryDatastore.
func NewMemoryDatastore(conf map[string]string) (Datastore, error) {
	return &MemoryDatastore{
		RWMutex: &sync.RWMutex{},
		Cars:    make(map[string]*Car),
		Cats:    make(map[string]*Cat),
	}, nil
}
