package models_test

import (
	"testing"

	"bitbucket.org/matoski/db-example/models"
)

func TestCatToString(t *testing.T) {
	testCases := []struct {
		desc string
		cat  *models.Cat
	}{
		{
			desc: "Puss",
			cat:  &models.Cat{ID: "001", Name: "Puss", Type: "Persian"},
		},
		{
			desc: "Smokey",
			cat:  &models.Cat{ID: "002", Name: "Smokey", Type: "Russian Blue"},
		},
		{
			desc: "empty",
			cat:  &models.Cat{},
		},
		{
			desc: "nil",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			actual := []byte(tC.cat.ToString())
			equals(t, expected(t, actual), actual)
		})
	}
}
