package models

import (
	"errors"
	"fmt"
)

// Cat entity
type Cat struct {
	ID   string
	Name string
	Type string
}

// ToString converts Cat object to string.
func (c *Cat) ToString() string {
	if c == nil || len(c.ID) == 0 {
		return ""
	}

	return fmt.Sprintf("%s is %s cat.", c.Name, c.Type)
}

// nolint
// CatNotFoundError error.
var CatNotFoundError = errors.New("Cat not found")

// nolint
// CatAlreadyExistsError error.
var CatAlreadyExistsError = errors.New("Cat already exists")

// CatDatastore interface provides the cat datastore operations.
type CatDatastore interface {
	AddCat(c *Cat) error
	FindCat(id string) (*Cat, error)
	UpdateCat(c *Cat) error
	RemoveCat(c *Cat) error
	GetCats() ([]*Cat, error)
}
