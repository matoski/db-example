package models_test

import (
	"testing"

	"bitbucket.org/matoski/db-example/models"
)

func TestCarToString(t *testing.T) {
	testCases := []struct {
		desc string
		car  *models.Car
	}{
		{
			desc: "Mercedes",
			car:  &models.Car{RegNumber: "DW69XLO", Make: "Mercedes", Model: "Benz"},
		},
		{
			desc: "Aston",
			car:  &models.Car{RegNumber: "MATO1", Make: "Aston", Model: "Martin"},
		},
		{
			desc: "empty",
			car:  &models.Car{},
		},
		{
			desc: "nil",
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			actual := []byte(tC.car.ToString())
			equals(t, expected(t, actual), actual)
		})
	}
}
