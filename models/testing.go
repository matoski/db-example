package models

// UnregisterAllDatastoreFactories will empty the datastore factories.
func UnregisterAllDatastoreFactories() {
	factoriesMx.Lock()
	defer factoriesMx.Unlock()
	// For tests.
	datastoreFactories = make(map[string]DatastoreFactory)
}
