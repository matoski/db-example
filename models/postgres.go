package models

import (
	"database/sql"
	"errors"
	"log"

	pq "github.com/lib/pq" // Register postgres driver
)

// PostgreSQLDatastore implements the XDatastore interfaces.
type PostgreSQLDatastore struct {
	DB *sql.DB
}

// Name returns the name of the Datastore.
func (pds *PostgreSQLDatastore) Name() string {
	return "postgres"
}

//#region implement CarDatastore interface

// AddCar inserts the Car in the Datastore.
func (pds *PostgreSQLDatastore) AddCar(c *Car) error {
	var tmp string
	err := pds.DB.QueryRow(
		"INSERT INTO cars(RegNumber, Make, Model) VALUES($1, $2, $3) RETURNING RegNumber",
		c.RegNumber, c.Make, c.Model,
	).Scan(&tmp)
	if err != nil {
		if err.(*pq.Error).Code.Name() == "unique_violation" {
			return CarAlreadyExistsError
		}
		return err
	}
	return nil
}

// FindCar returns the Car object for given regNumber.
func (pds *PostgreSQLDatastore) FindCar(regNumber string) (*Car, error) {
	var c Car
	err := pds.DB.QueryRow(
		"SELECT RegNumber, Make, Model FROM cars WHERE RegNumber=$1", regNumber,
	).Scan(&c.RegNumber, &c.Make, &c.Model)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, CarNotFoundError
		}
		return nil, err
	}
	return &c, nil
}

// UpdateCar will update the Datastore with the Car object.
func (pds *PostgreSQLDatastore) UpdateCar(c *Car) error {
	var tmp string
	err := pds.DB.QueryRow(
		"UPDATE cars SET Make=$2, Model=$3 WHERE RegNumber=$1 RETURNING RegNumber",
		c.RegNumber, c.Make, c.Model,
	).Scan(&tmp)
	if err != nil {
		if err == sql.ErrNoRows {
			return CarNotFoundError
		}
		return err
	}
	return nil
}

// RemoveCar removes the Car from the Datastore.
func (pds *PostgreSQLDatastore) RemoveCar(c *Car) error {
	var tmp string
	err := pds.DB.QueryRow(
		"DELETE FROM cars WHERE RegNumber=$1 RETURNING RegNumber", c.RegNumber,
	).Scan(&tmp)
	if err != nil {
		if err == sql.ErrNoRows {
			return CarNotFoundError
		}
		return err
	}
	return nil

}

// GetCars returns all of the Cars from the Datastore.
func (pds *PostgreSQLDatastore) GetCars() ([]*Car, error) {
	rows, err := pds.DB.Query("SELECT RegNumber, Make, Model FROM cars")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	cars := make([]*Car, 0)
	for rows.Next() {
		c := new(Car)
		err = rows.Scan(&c.RegNumber, &c.Make, &c.Model)
		if err != nil {
			return nil, err
		}
		cars = append(cars, c)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return cars, nil
}

//#endregion

//#region implement CatDatastore interface

// AddCat inserts the Cat in the Datastore.
func (pds *PostgreSQLDatastore) AddCat(c *Cat) error {
	var tmp string
	err := pds.DB.QueryRow(
		"INSERT INTO cats(ID, Name, Type) VALUES($1, $2, $3) RETURNING ID",
		c.ID, c.Name, c.Type,
	).Scan(&tmp)
	if err != nil {
		if err.(*pq.Error).Code.Name() == "unique_violation" {
			return CatAlreadyExistsError
		}
		return err
	}
	return nil
}

// FindCat returns the Cat object for given regNumber.
func (pds *PostgreSQLDatastore) FindCat(regNumber string) (*Cat, error) {
	var c Cat
	err := pds.DB.QueryRow(
		"SELECT ID, Name, Type FROM cats WHERE ID=$1", regNumber,
	).Scan(&c.ID, &c.Name, &c.Type)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, CatNotFoundError
		}
		return nil, err
	}
	return &c, nil
}

// UpdateCat will update the Datastore with the Cat object.
func (pds *PostgreSQLDatastore) UpdateCat(c *Cat) error {
	var tmp string
	err := pds.DB.QueryRow(
		"UPDATE cats SET Name=$2, Type=$3 WHERE ID=$1 RETURNING ID",
		c.ID, c.Name, c.Type,
	).Scan(&tmp)
	if err != nil {
		if err == sql.ErrNoRows {
			return CatNotFoundError
		}
		return err
	}
	return nil
}

// RemoveCat removes the Cat from the Datastore.
func (pds *PostgreSQLDatastore) RemoveCat(c *Cat) error {
	var tmp string
	err := pds.DB.QueryRow(
		"DELETE FROM cats WHERE ID=$1 RETURNING ID", c.ID,
	).Scan(&tmp)
	if err != nil {
		if err == sql.ErrNoRows {
			return CatNotFoundError
		}
		return err
	}
	return nil

}

// GetCats returns all of the Cats from the Datastore.
func (pds *PostgreSQLDatastore) GetCats() ([]*Cat, error) {
	rows, err := pds.DB.Query("SELECT ID, Name, Type FROM cats")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	cats := make([]*Cat, 0)
	for rows.Next() {
		c := new(Cat)
		err = rows.Scan(&c.ID, &c.Name, &c.Type)
		if err != nil {
			return nil, err
		}
		cats = append(cats, c)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return cats, nil
}

//#endregion

// NewPostgreSQLDatastore creates PostgreSQLDatastore.
func NewPostgreSQLDatastore(conf map[string]string) (Datastore, error) {
	dsn, ok := conf["DATASTORE_POSTGRES_DSN"]
	if !ok {
		return nil, errors.New("DATASTORE_POSTGRES_DSN is required for postgres datastore")
	}
	log.Printf("creating postgres datastore with DSN=[%s]", dsn)
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	// HACK: Make sure the tables are created
	log.Print("creating tables...")
	var dummy string
	err = db.QueryRow(`CREATE TABLE IF NOT EXISTS cars (
		RegNumber VARCHAR(20) CONSTRAINT reg_num_key PRIMARY KEY,
		Make VARCHAR(40) NOT NULL,
		Model VARCHAR(40) NOT NULL
	)`).Scan(&dummy)
	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
	}
	log.Print("created cars table")
	err = db.QueryRow(`CREATE TABLE IF NOT EXISTS cats (
		ID VARCHAR(20) CONSTRAINT id_key PRIMARY KEY,
		Name VARCHAR(40) NOT NULL,
		Type VARCHAR(40) NOT NULL
	)`).Scan(&dummy)
	if err != nil {
		if err != sql.ErrNoRows {
			return nil, err
		}
	}
	log.Print("created cats table")
	return &PostgreSQLDatastore{db}, nil
}
