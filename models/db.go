package models

import (
	"fmt"
	"sort"
	"strings"
	"sync"
)

// Datastore interface defines the common operations for a Datastore.
type Datastore interface {
	Name() string
	CarDatastore
	CatDatastore
}

// DatastoreFactory creates Datastore objects.
type DatastoreFactory func(conf map[string]string) (Datastore, error)

// nolint
var (
	factoriesMx        sync.RWMutex
	datastoreFactories = make(map[string]DatastoreFactory)
)

// Register makes a datastore factory available by the provided name.
func Register(name string, factory DatastoreFactory) {
	factoriesMx.Lock()
	defer factoriesMx.Unlock()
	if factory == nil {
		panic("db: Register factory is nil")
	}
	if _, dup := datastoreFactories[name]; dup {
		panic("db: Register called twice for factory " + name)
	}
	datastoreFactories[name] = factory
}

// DatastoreFactories returns a sorted list of the names of the registered datastore factories.
func DatastoreFactories() []string {
	factoriesMx.RLock()
	defer factoriesMx.RUnlock()
	var list []string
	for name := range datastoreFactories {
		list = append(list, name)
	}
	sort.Strings(list)
	return list
}

// nolint
func init() {
	Register("postgres", NewPostgreSQLDatastore)
	Register("memory", NewMemoryDatastore)
}

// CreateDatastore creates a Datastore for the provided engine using the DATASTORE config.
func CreateDatastore(conf map[string]string) (Datastore, error) {
	// Read the configuration for datastore or default to "memory".
	engineName, ok := conf["DATASTORE"]
	if !ok {
		engineName = "memory"
	}
	engineFactory, ok := datastoreFactories[engineName]
	if !ok {
		// Factory has not been registered.
		// Make a list of all available datastore factories for logging.
		availableDatastores := DatastoreFactories()
		return nil, fmt.Errorf("Invalid datastore name. Must be one of: %s",
			strings.Join(availableDatastores, ", "))
	}
	// Run the factory with the configuration. (MAGIC)
	return engineFactory(conf)
}
