package models

import (
	"errors"
	"fmt"
)

// Car entity
type Car struct {
	RegNumber string
	Make      string
	Model     string
}

// ToString converts Car object to string.
func (c *Car) ToString() string {
	if c == nil || len(c.RegNumber) == 0 {
		return ""
	}

	return fmt.Sprintf("%s %s with registration number %s.", c.Make, c.Model, c.RegNumber)
}

// nolint
// CarNotFoundError error.
var CarNotFoundError = errors.New("Car not found")

// nolint
// CarAlreadyExistsError error.
var CarAlreadyExistsError = errors.New("Car already exists")

// CarDatastore interface provides the car datastore operations.
type CarDatastore interface {
	AddCar(c *Car) error
	FindCar(regNumber string) (*Car, error)
	UpdateCar(c *Car) error
	RemoveCar(c *Car) error
	GetCars() ([]*Car, error)
}
