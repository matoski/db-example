package models_test

import (
	"testing"

	"bitbucket.org/matoski/db-example/models"
)

var (
	mockDatastore = &MockDatastore{}
	mockConf      = map[string]string{"DATASTORE": "mock"}
)

// nolint
// Empty factory register
func init() {
	models.UnregisterAllDatastoreFactories()
}

type MockDatastore struct {
	*models.MemoryDatastore
}

func (tds *MockDatastore) Name() string {
	return "mock"
}

func NewMockDatastore(conf map[string]string) (models.Datastore, error) {
	return mockDatastore, nil // return mockDatastore
}

func TestRegisterNilFactory(t *testing.T) {
	defer func() {
		r := recover()
		assert(t, r != nil, "registering a nil factory should panic")
		equals(t, "db: Register factory is nil", r.(string))
	}()
	models.Register("test", nil)
}

func TestRegisterFactoryTwice(t *testing.T) {
	defer models.UnregisterAllDatastoreFactories()
	models.Register("test", NewMockDatastore)
	defer func() {
		models.UnregisterAllDatastoreFactories()
		r := recover()
		assert(t, r != nil, "registering the same factory twice should panic")
		equals(t, "db: Register called twice for factory test", r.(string))
	}()
	models.Register("test", NewMockDatastore)
}

func TestRegisterFactory(t *testing.T) {
	defer models.UnregisterAllDatastoreFactories()
	models.Register("mock", NewMockDatastore)
	factories := models.DatastoreFactories()
	assert(t, len(factories) == 1, "mock factory should be registered")
	equals(t, "mock", factories[0])
}

func TestRegisterTwoFactories(t *testing.T) {
	defer models.UnregisterAllDatastoreFactories()
	models.Register("mock1", NewMockDatastore)
	models.Register("mock2", NewMockDatastore)
	factories := models.DatastoreFactories()
	assert(t, len(factories) == 2, "two factories should be registered")
	equals(t, "mock1", factories[0])
	equals(t, "mock2", factories[1])
}

func TestCreateDatastore(t *testing.T) {
	defer models.UnregisterAllDatastoreFactories()
	models.Register("mock", NewMockDatastore)
	act, err := models.CreateDatastore(mockConf) // should call NewMockDatastore
	ok(t, err)
	equals(t, mockDatastore, act)
}

func TestCreatingNonRegisteredDatastore(t *testing.T) {
	defer models.UnregisterAllDatastoreFactories()
	models.Register("test", NewMockDatastore)
	ds, err := models.CreateDatastore(mockConf) // mockConfig has DATASTORE=mock
	assert(t, ds == nil, "it should not return a datastore for unregistered engine")
	equals(t, "Invalid datastore name. Must be one of: test", err.Error())
}

func TestCreateDatastoreDefault(t *testing.T) {
	defer models.UnregisterAllDatastoreFactories()
	models.Register("memory", NewMockDatastore)             // make sure default datastore is registered
	act, err := models.CreateDatastore(map[string]string{}) // should call NewMockDatastore
	ok(t, err)
	equals(t, mockDatastore, act)
}
