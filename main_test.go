package main

import (
	"bytes"
	"errors"
	"testing"

	"bitbucket.org/matoski/db-example/models"
)

var ErrNotSupported = errors.New("not supported")

var (
	addedCars   []*models.Car = make([]*models.Car, 0)
	removedCars []*models.Car = make([]*models.Car, 0)
	getCars     []*models.Car = []*models.Car{
		&models.Car{
			RegNumber: "1",
			Make:      "make1",
			Model:     "model1",
		},
		&models.Car{
			RegNumber: "2",
			Make:      "make2",
			Model:     "model2",
		},
	}
	datastore models.Datastore = &MockDatastore{}
)

type MockDatastore struct{}

func (m *MockDatastore) Name() string { return "mock" }

func (m *MockDatastore) AddCar(c *models.Car) error {
	addedCars = append(addedCars, c)
	return nil
}
func (m *MockDatastore) FindCar(regNumber string) (*models.Car, error) { return nil, ErrNotSupported }
func (m *MockDatastore) UpdateCar(c *models.Car) error                 { return ErrNotSupported }
func (m *MockDatastore) GetCars() ([]*models.Car, error) {
	return getCars, nil
}
func (m *MockDatastore) RemoveCar(c *models.Car) error {
	removedCars = append(removedCars, c)
	return nil
}
func (m *MockDatastore) AddCat(c *models.Cat) error             { return ErrNotSupported }
func (m *MockDatastore) FindCat(id string) (*models.Cat, error) { return nil, ErrNotSupported }
func (m *MockDatastore) UpdateCat(c *models.Cat) error          { return ErrNotSupported }
func (m *MockDatastore) RemoveCat(c *models.Cat) error          { return ErrNotSupported }
func (m *MockDatastore) GetCats() ([]*models.Cat, error)        { return nil, ErrNotSupported }

func TestAppLogic(t *testing.T) {
	var buf bytes.Buffer
	AppLogic(&buf, datastore)

	assert(t, len(addedCars) == 3, "there should be three cars added")
	equals(t, "DW69XLO", addedCars[0].RegNumber)
	equals(t, "MATO1", addedCars[1].RegNumber)
	equals(t, "FD19YIL", addedCars[2].RegNumber)

	actual := buf.Bytes()
	equals(t, expected(t, actual), actual)

	assert(t, len(removedCars) == 2, "there should be two cars removed")
	equals(t, "1", removedCars[0].RegNumber)
	equals(t, "2", removedCars[1].RegNumber)
}
